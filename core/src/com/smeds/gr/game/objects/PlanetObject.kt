package com.smeds.gr.game.objects

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.Vector2


/**
 * Created by patsm159 on 2017-11-12.
 */
class PlanetObject(val x: Float, val y: Float, val r: Float): AbstractGameObject() {

    constructor(x: Double, y: Double, r: Float) : this(x.toFloat(),y.toFloat(),r) {}

    private val shapeRenderer: ShapeRenderer = ShapeRenderer()

    private val pixmap: Pixmap = Pixmap(256,256,Pixmap.Format.RGBA8888);
    private var texture: Texture;
    private var diameter: Float = 2*r;
    var center: Vector2 = Vector2(x,y);

    init {
        texture = Texture(pixmap);
        position.set(center.x -r,center.y - r)
        pixmap.setColor(0.0f, 1.0f, 0.0f, 1.0f)
        pixmap.fillCircle(256/2, 256/2, 256/2);
        texture.draw(pixmap, 0, 0);
        texture.bind();
        origin.set(r,r)
        dimension.set(diameter,diameter)
        scale.set(diameter,diameter)
        gravity = 1.0f
        mass = 1.47f
        bounds.set(position.x,position.y,diameter,diameter)
    }

    fun draw_boundary_box(srend: ShapeRenderer, deltaTime: Float) {
        srend.setColor(Color.BLUE)
        srend.rect(position.x,position.y,1.0f,1.0f)
    }

    override fun render(batch: SpriteBatch, deltaTime: Float) {
        batch.draw(texture,
                    position.x, position.y,
                    origin.x, origin.y,
                    dimension.x, dimension.y,
                    scale.x, scale.y,
                    rotation,
                    0,0,
                    256,256,
                    false,false)
    }

    override fun render(shapeRenderer: ShapeRenderer, deltaTime: Float) {}
}