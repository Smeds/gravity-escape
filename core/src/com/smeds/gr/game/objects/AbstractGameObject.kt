package com.smeds.gr.game.objects

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import javax.xml.bind.util.ValidationEventCollector

/**
 * Created by patsm159 on 2017-11-11.
 */

abstract class AbstractGameObject {
    var position: Vector2 = Vector2()
    var dimension: Vector2 = Vector2(1.0f,1.0f)
    var origin: Vector2 = Vector2()
    var scale: Vector2 = Vector2(1.0f,1.0f)
    var rotation: Float = 0.0f

    var velocity: Vector2 = Vector2()
    var terminalVelocity: Vector2? = Vector2(1.0f,1.0f)
    var friction: Vector2 = Vector2(0f,0f)
    var acceleration: Vector2 = Vector2()
    var gravity: Float = 0.0f
    var mass: Float = 1.0f
    var moveable = false
    var appliedForce: Vector2 = Vector2()
    var momentum: Vector2 = Vector2()
    var bounds: Rectangle = Rectangle()

    fun hasGravity(): Boolean {
        return (gravity > 0.0001f)
    }

    fun isMoveable(): Boolean {
        return moveable
    }

    fun addForce(force: Vector2) {
        this.appliedForce.add(force)
    }

    open fun update(deltaTime: Float) {
        this.position.x += this.momentum.x * deltaTime / this.mass
        this.position.y += this.momentum.y * deltaTime / this.mass
    }

    abstract fun render(batch: SpriteBatch, deltaTime: Float)

    abstract fun render(shapeRenderer: ShapeRenderer, deltaTime: Float)
}