package com.smeds.gr.game.objects

import com.smeds.gr.game.gui.LeftButtonListener
import com.smeds.gr.game.gui.RightButtonListener
import com.smeds.gr.game.gui.ThrottleButtonListener

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Polygon
import com.badlogic.gdx.math.Vector2
import com.smeds.gr.game.gui.Button

/**
 * Created by patsm159 on 2017-11-12.
 */

interface ButtonHandler {
    fun pressed(): Boolean
}

class RocketObject( val x: Float, val y: Float, val w: Int, val h: Int) : AbstractGameObject() {
    private val shapeRenderer: ShapeRenderer = ShapeRenderer()

    private var ship_texture: Texture;
    private var flame_texture: Texture;
    var rocker_on: Boolean = false
    var direction: Vector2 = Vector2(0.0f,1.0f)
    var rotate_force: Float = 0f
    var flame_scale: Vector2
    var flame_dimension: Vector2 = Vector2(1.0f,1.0f)
    var scale_up: Float = 1.0f
    var scale_time: Float = 0.3f
    var scale_current: Float = 1.0f
    var bounds_extra: Float = 0.1f
    var color: Color = Color.BLUE
    var polygon: Polygon = Polygon()
    var turn_left: Boolean = false
    var turn_right: Boolean = false
    var throttle: Float = 0f
    var maxSpeed: Float = 0.8f

    private lateinit var leftTurnHandler: ButtonHandler
    private lateinit var rightTurnHandler: ButtonHandler
    private lateinit var thrustHandler: ButtonHandler

    init {
        rotation = 0f
        position.set(x,y)
        ship_texture = Texture(Gdx.files.internal("images/space.png"))
        ship_texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)

        flame_texture = Texture(Gdx.files.internal("images/flame.png"))
        flame_texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
        origin.set(1.0f/2f,1.0f/2f)
        dimension.set(1.0f,1.0f)
        scale.set(0.5f,1f)
        flame_scale = Vector2(0.5f*24.0f/116.0f, 88.0f/256.0f )
        moveable = true
        val speed = 0.0f//Math.sqrt(1.47/2.75)
        this.momentum.x = 0f
        this.momentum.y = 0f//(this.mass*speed).toFloat()
        bounds.set(position.x - bounds_extra,position.y - bounds_extra,1.0f + bounds_extra*2,1.0f + bounds_extra*2)

        polygon = Polygon(floatArrayOf(
                0.51f, 0f,
                0.63f, 0.19f,
                0.66f, 0.38f,
                0.61f, 0.68f,
                0.705f, 0.755f,
                0.75f, 1.0f,
                0.55f, 0.92f,
                0.45f, 0.92f,
                0.25f, 1.0f,
                0.295f, 0.755f,
                0.39f, 0.68f,
                0.34f, 0.38f,
                0.37f, 0.19f,
                0.49f, 0f));
        polygon.setOrigin(origin.x,origin.y)
        polygon.setPosition(position.x,position.y)
        polygon.setRotation(this.rotation)

    }
    override fun render(batch: SpriteBatch, deltaTime: Float) {
        if(this.throttle > 0) {
            var dimension_new = scale_up*this.throttle
            batch.draw(flame_texture,
                    position.x , //The left of image
                    position.y + flame_dimension.y -0.1f,  //The bottom of image
                    origin.x,  //Pivot Point(X), used for rotating the image
                    origin.y-flame_dimension.y +0.1f,
                    dimension.x,
                    dimension.y*dimension_new,
                    scale.x*0.2f, scale.y,
                    rotation+this.rotate_force,
                    0,0,
                    24,88,
                    false,true)
        }
        batch.draw(ship_texture,
                position.x, //The left of image
                position.y, //The bottom of image
                origin.x, //Pivot Point(X), used for rotating the image
                origin.y, //Pivot Point(X), used for rotating the image
                dimension.x, //The final size(Width) of the image part to be drawn
                dimension.y, //The final size(Width) of the image part to be drawn
                scale.x, //Scale in x dimension
                scale.y, //Scale in y dimension
                rotation+this.rotate_force,
                0,0,
                116,256,
                false,true)
    }

    fun update_polygon() {
        polygon.setPosition(position.x,position.y)
        polygon.setRotation(this.rotation+this.rotate_force)
    }

    fun draw_boundary_box(srend: ShapeRenderer, deltaTime: Float) {
        srend.polygon(polygon.getTransformedVertices());
        srend.setColor(this.color)
        srend.rect(bounds.x,bounds.y,bounds.width,bounds.height)
    }

    fun setLeftTurnHandler(handler: ButtonHandler) {
        this.leftTurnHandler = handler
    }

    fun setRightTurnHandler(handler: ButtonHandler) {
        this.rightTurnHandler = handler
    }

    fun setThrustHandler(handler: ButtonHandler) {
        this.thrustHandler = handler
    }

    fun collision(collided: Int) {
        if(collided == 1) {
            color = Color.RED
        } else if(collided == 2) {
            color = Color.PINK
        }
        else {
            color = Color.BLUE
        }
    }

    fun draw_line(srend: ShapeRenderer, deltaTime: Float) {
        srend.setColor(Color.RED)
        srend.line(0.0f,0.0f,0f,position.x + origin.x, position.y + origin.y, 0f)//, momentum.x, momentum.y, 5f);
        srend.line(position.x + origin.x, position.y + origin.y, 0f, position.x + origin.x + momentum.x, position.y + origin.y + momentum.y, 0f);
        srend.setColor(Color.BLUE)
        srend.line(position.x + origin.x, position.y + origin.y, 0f,
                  position.x + origin.x + direction.x,
                  position.y + origin.y + direction.y, 0f);

    }

    override fun render(shapeRenderer: ShapeRenderer, deltaTime: Float) {}

    override fun update(deltaTime: Float) {
        if (this.leftTurnHandler.pressed()) {
            rotateCounterClockwise()
        }
        if(this.rightTurnHandler.pressed()) {
            rotateClockwise()
        }

        var old_momentum = Vector2(momentum)

        momentum.add(this.appliedForce.x*deltaTime,this.appliedForce.y*deltaTime)
        this.rotate_force = Math.toDegrees(Math.atan2((momentum.y-old_momentum.y).toDouble(), (momentum.x-old_momentum.x).toDouble())).toFloat()
        direction.setAngle(this.rotation+this.rotate_force-90)

        if(this.thrustHandler.pressed()) {
            this.throttle = 0.5f
            direction.nor()
            this.appliedForce.x += direction.x*this.maxSpeed*this.throttle
            this.appliedForce.y += direction.y*this.maxSpeed*this.throttle
            momentum.add(this.appliedForce.x*deltaTime,this.appliedForce.y*deltaTime)
        } else {
            this.throttle = 0.0f
        }
        scale_current += deltaTime
        super.update(deltaTime)
        bounds.set(position.x - bounds_extra,position.y - bounds_extra,1.0f + bounds_extra*2,1.0f + bounds_extra*2)
        update_polygon()
    }

    fun rotateClockwise() {
        direction.rotate(1f)
        rotation += 1f
        when {
            Math.abs(360f - rotation) < 0.01f -> {
                rotation = 0f
                direction.set(0f,1f)
            }
            Math.abs(rotation - 90f) < 0.01 -> {
                rotation = 90f
                direction.set(1f, 0f)
            }
            Math.abs(rotation - 180f) < 0.01 -> {
                direction.set(0f, -1f)
                rotation = 180f
            }
            Math.abs(rotation - 270f) < 0.01 -> {
                direction.set(0f, -1f)
                rotation = 270f
            }
        }
    }

    fun rotateCounterClockwise() {
        direction.rotate(-1f)
        rotation -= 1f
        when {
            rotation < 0.01f -> {
                rotation = 360f
                direction.set(0f, 1f)
            }
            Math.abs(rotation - 90f) < 0.01 -> {
                direction.set(1f, 0f)
                rotation = 90f
            }
            Math.abs(rotation - 180f) < 0.01 -> {
                direction.set(0f, -1f)
                rotation = 180f
            }
            Math.abs(rotation - 270f) < 0.01 -> {
                direction.set(0f, 1f)
                rotation = 270f
            }
        }
    }
}