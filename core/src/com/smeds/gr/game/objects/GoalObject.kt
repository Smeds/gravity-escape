package com.smeds.gr.game.objects

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.Vector2


/**
 * Created by patsm159 on 2017-11-12.
 */
class GoalObject(val x: Float, val y: Float, val r: Float): AbstractGameObject() {

    constructor(x: Double, y: Double, r: Float) : this(x.toFloat(),y.toFloat(),r) {}

    private val shapeRenderer: ShapeRenderer = ShapeRenderer()

    private var diameter: Float = 2*r;
    var center: Vector2 = Vector2(x,y);
    var timeElapsed: Float = 0f
    val deltaTime: Float = 1f/5f
    var counter: Int = 0
    private var colors: Array<FloatArray> = arrayOf(
            floatArrayOf(78f/255f,111f/255f,62f/255f),
            floatArrayOf(159f/255f,192f/255f,64f/255f))

    private var colors_light: Array<FloatArray> = arrayOf(
            floatArrayOf(123f/255f,152f/255f,40f/255f),
            floatArrayOf(210f/255f,242f/255f,152f/255f))

    init {
        position.set(center.x -r,center.y - r)
        origin.set(r,r)
        dimension.set(diameter,diameter)
        scale.set(diameter,diameter)
        gravity = 1.0f
        mass = 1.47f
        bounds.set(position.x,position.y,diameter,diameter)
    }

    fun draw_boundary_box(srend: ShapeRenderer) {
        srend.setColor(Color.BLUE)
        srend.rect(position.x,position.y,1.0f,1.0f)
    }

    override fun render(batch: SpriteBatch, deltaTime: Float) {}

    override fun render(shapeRenderer: ShapeRenderer, deltaTime: Float) {
        this.timeElapsed += deltaTime
        if(this.timeElapsed > this.deltaTime) {
            this.counter += 1
            this.timeElapsed -= this.deltaTime
            if(this.counter > 4) {
                this.counter = 0
            }
        }
        shapeRenderer.setColor(this.colors[0][0], this.colors[0][1], this.colors[0][2], 1.0f)
        shapeRenderer.circle(this.position.x+this.r,this.position.y+this.r,this.r,100)
        if(counter == 0) {
            shapeRenderer.setColor(this.colors_light[1][0], this.colors_light[1][1], this.colors_light[1][2], 1.0f)
        } else {
            shapeRenderer.setColor(this.colors[1][0], this.colors[1][1], this.colors[1][2], 1.0f)
        }
        shapeRenderer.circle(this.position.x+this.r,this.position.y+this.r,6*this.r/7,100)
        if(counter == 1) {
            shapeRenderer.setColor(this.colors_light[0][0], this.colors_light[0][1], this.colors_light[0][2], 1.0f)
        } else {
            shapeRenderer.setColor(this.colors[0][0], this.colors[0][1], this.colors[0][2], 1.0f)
        }
        shapeRenderer.circle(this.position.x+this.r,this.position.y+this.r,5*this.r/7,100)
        if(counter == 2) {
            shapeRenderer.setColor(this.colors_light[1][0], this.colors_light[1][1], this.colors_light[1][2], 1.0f)
        } else {
            shapeRenderer.setColor(this.colors[1][0], this.colors[1][1], this.colors[1][2], 1.0f)
        }
        shapeRenderer.circle(this.position.x+this.r,this.position.y+this.r,4*this.r/7,100)
        if(counter == 3) {
            shapeRenderer.setColor(this.colors_light[0][0], this.colors_light[0][1], this.colors_light[0][2], 1.0f)
        } else {
            shapeRenderer.setColor(this.colors[0][0], this.colors[0][1], this.colors[0][2], 1.0f)
        }
        shapeRenderer.circle(this.position.x+this.r,this.position.y+this.r,3*this.r/7,100)
        if(counter == 4) {
            shapeRenderer.setColor(this.colors_light[1][0], this.colors_light[1][1], this.colors_light[1][2], 1.0f)
        } else {
            shapeRenderer.setColor(this.colors[1][0], this.colors[1][1], this.colors[1][2], 1.0f)
        }
        shapeRenderer.circle(this.position.x+this.r,this.position.y+this.r,2*this.r/7,100)
        shapeRenderer.setColor(this.colors[0][0], this.colors[0][1], this.colors[0][2], 1.0f)
        shapeRenderer.circle(this.position.x+this.r,this.position.y+this.r,this.r/7,100)
    }
}