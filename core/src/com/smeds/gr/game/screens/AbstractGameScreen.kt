package com.smeds.gr.game.screens

import com.badlogic.gdx.Game
import com.badlogic.gdx.Screen


/**
 * Created by patsm159 on 2018-04-02.
 */
open abstract class AbstractGameScreen(protected var game: Game) : Screen {

    abstract override fun render(deltaTime: Float)
    abstract override fun resize(width: Int, height: Int)
    abstract override fun show()
    abstract override fun hide()
    abstract override fun pause()
    override fun resume() {}

    override fun dispose() {}
}