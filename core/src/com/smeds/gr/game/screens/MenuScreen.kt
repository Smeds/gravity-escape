package com.smeds.gr.game.screens

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.smeds.gr.game.utils.Constants.VIEWPORT_GUI_HEIGHT
import com.smeds.gr.game.utils.Constants.VIEWPORT_GUI_WIDTH
import java.util.*
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.ui.Stack
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.smeds.gr.game.utils.Constants
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable


/**
 * Created by patsm159 on 2018-04-02.
 */
class MenuScreen(game: Game) : AbstractGameScreen(game) {
    private val random = Random(100)
    private val planetColor = floatArrayOf(49f/255f, 178f/255f, 130f/255f)
    private val moonColor = floatArrayOf(255f/255f, 125f/255f, 125f/255f)
    private val backgroundColor = floatArrayOf(21f/255, 21f/255f, 21f/255f)//floatArrayOf(0f/255, 68f/255f, 133f/255f)
    private val starColor = floatArrayOf(223f/255, 176f/255f, 0f/255f)
    private val numberOfStars: Int = 200
    private val numberOfCoords: Int
    private var starPositions: FloatArray
    private var shapeRenderer: ShapeRenderer
    private var moon1Rotation = 0f
    private var moon2Rotation = 45f
    private val moon2RoationPerSecond = 10f
    private val moon1RoationPerSecond = 15f
    private var  skin: Skin
    private lateinit var stage: Stage
    private var camera: Camera
    private lateinit var viewport: Rectangle
    private var font: BitmapFont


    fun getScreenSizeInches(): Float {
        //According to LibGDX documentation; getDensity() returns a scalar value for 160dpi.
        val dpi = 160 * Gdx.graphics.density
        val widthInches = Gdx.graphics.width / dpi
        val heightInches = Gdx.graphics.height / dpi

        //Use the pythagorean theorem to get the diagonal screen size
        return Math.sqrt(Math.pow(widthInches.toDouble(), 2.0) + Math.pow(heightInches.toDouble(), 2.0)).toFloat()
    }

    init {
        camera = OrthographicCamera(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT)
        shapeRenderer = ShapeRenderer()
        this.numberOfCoords = this.numberOfStars*2
        starPositions = FloatArray(this.numberOfCoords)
        for (i in 0..this.numberOfCoords-1 step 2) {
            starPositions[i] = rand()* Constants.VIEWPORT_GUI_WIDTH
            starPositions[i + 1] = rand()* Constants.VIEWPORT_GUI_HEIGHT
        }
        skin = Skin()
        var generator = FreeTypeFontGenerator(Gdx.files.internal("fonts/raleway/Raleway-Bold.ttf"))
        var fontSize: Float = 60f
        if (getScreenSizeInches() < 7)
            fontSize = 24f
        font = createFont(generator,fontSize)
        skin.add("default-font",font,font.javaClass)
        var up = Texture(Gdx.files.internal("button.png"))
        var down = Texture(Gdx.files.internal("button-down.png"))
        skin.add("default-up", up, up.javaClass )
        skin.add("default-down", down, down.javaClass )
        generator.dispose()
        skin.load(Gdx.files.internal("uiskin.json"))
    }

    override fun render(deltaTime: Float) {
        camera.update()
        camera.apply{Gdx.gl20};

        Gdx.gl.glViewport(viewport.x.toInt(), viewport.y.toInt(), viewport.width.toInt(), viewport.height.toInt())
        Gdx.gl.glClearColor(this.backgroundColor[0], this.backgroundColor[1], this.backgroundColor[2], 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        shapeRenderer.projectionMatrix = camera.combined
        this.render_space(shapeRenderer,deltaTime)
        this.stage.act(deltaTime)
        this.stage.draw()
    }

    private fun rebuildStage() {
        stage.clear()
        val stack = Stack()
        stack.setSize(stage.viewport.screenWidth.toFloat(),stage.viewport.worldHeight.toFloat())
        stack.add(this.buildControlsLayer())
        stage.addActor(stack)
        this.buildControlsLayer()
    }

    fun createFont(ftfg: FreeTypeFontGenerator, dp: Float): BitmapFont {
        val parameter = FreeTypeFontParameter()
        parameter.size = (dp * Gdx.graphics.density).toInt()
        return ftfg.generateFont(parameter)
    }
//On Init

    private fun createButtons(text: String, skin: Skin, listener: ChangeListener): TextButton {
        val button: TextButton = TextButton(text,skin)
        button.padLeft(20f)
        button.padRight(20f)
        button.addListener(listener)
        return button
    }

    private fun buildControlsLayer(): Table {
        val layer = Table()
        layer.debug =true

        layer.add(
            createButtons("New Game", skin, object: ChangeListener() {
                override fun changed(event: ChangeListener.ChangeEvent, actor: Actor) { game.setScreen(GameScreen(game)) }}))

        layer.row()
        layer.add(
            createButtons("Quit",skin, object : ChangeListener() {
                override fun changed(event: ChangeListener.ChangeEvent, actor: Actor) { Gdx.app.exit() }
        }))
        return layer
    }

    fun render_space(shapeRenderer: ShapeRenderer,deltaTime: Float) {
        this.moon1Rotation += deltaTime*this.moon1RoationPerSecond
        if(this.moon1Rotation > 360f) {
            this.moon1Rotation -= 360f
        }
        this.moon2Rotation += deltaTime*this.moon2RoationPerSecond
        if(this.moon2Rotation > 360f) {
            this.moon2Rotation -= 360f
        }
        Gdx.gl20.glLineWidth(2f)
        shapeRenderer.identity()
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line)
        generateStars(shapeRenderer)
        shapeRenderer.setColor(this.moonColor[0], this.moonColor[1], this.moonColor[2], 1.0f)
        shapeRenderer.circle(100f,100f,78f,200)
        shapeRenderer.circle(100f,100f,134f,200)

        shapeRenderer.end()
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
        shapeRenderer.setColor(this.planetColor[0], this.planetColor[1], this.planetColor[2], 1.0f)
        shapeRenderer.circle(100f,100f,50f,200)
        shapeRenderer.setColor(this.moonColor[0], this.moonColor[1], this.moonColor[2], 1.0f)
        shapeRenderer.identity()
        shapeRenderer.translate(100f,100f,0f)
        shapeRenderer.rotate(0f,0f,1f, this.moon1Rotation)
        shapeRenderer.circle(55f,55f,10f,100)
        shapeRenderer.rotate(0f,0f,1f, this.moon2Rotation)
        shapeRenderer.circle(95f,95f,15f,100)
        shapeRenderer.end()
    }


    fun rand() : Float {
        return random.nextFloat() - 0.5f
    }

    private fun generateStars(shapeRenderer: ShapeRenderer) {
        shapeRenderer.setColor(this.starColor[0], this.starColor[1], this.starColor[2],1f)
        for (i in 0..this.numberOfCoords-1 step 2) {
            shapeRenderer.point(this.starPositions[i], this.starPositions[i+1],0f)
        }
    }

    override fun resize(width: Int, height: Int) {
        val aspectRatio: Float = width.toFloat() / height.toFloat()
        var scale: Float = 1f
        val crop: Vector2 = Vector2(0f, 0f)

        if (aspectRatio > Constants.ASPECT_RATIO) {
            scale = height.toFloat() / Constants.VIEWPORT_GUI_HEIGHT
            crop.x = (width - Constants.VIEWPORT_GUI_WIDTH * scale) / 2f
        } else if (aspectRatio < Constants.ASPECT_RATIO) {
            scale = width.toFloat() / Constants.VIEWPORT_GUI_WIDTH
            crop.y = (height - Constants.VIEWPORT_GUI_HEIGHT * scale) / 2f
        } else {
            scale = width.toFloat() / Constants.VIEWPORT_GUI_WIDTH
        }

        val w = Constants.VIEWPORT_GUI_WIDTH * scale
        val h = Constants.VIEWPORT_GUI_HEIGHT * scale
        viewport = Rectangle(crop.x, crop.y, w, h)
        //stage.viewport.update(width,height,false)
    }

    override fun show() {
        stage = Stage()
        Gdx.input.inputProcessor = stage
        rebuildStage()
    }

    override fun hide() {
        stage.dispose()
    }

    override fun pause() {}
}