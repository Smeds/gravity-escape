package com.smeds.gr.game.screens

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.graphics.GL20
import com.smeds.gr.game.WorldController
import com.smeds.gr.game.WorldRender
import com.smeds.gr.game.gui.LeftTurn
import com.smeds.gr.game.gui.RightTurn
import com.smeds.gr.game.gui.UIInterface
import com.smeds.gr.game.objects.ButtonHandler

/**
 * Created by patsm159 on 2018-04-02.
 */
class GameScreen(game: Game) : AbstractGameScreen(game) {
    private lateinit var controller: WorldController
    private lateinit var renderer: WorldRender
    private lateinit var uiInteface: UIInterface
    private val backgroundColor = floatArrayOf(21f/255, 21f/255f, 21f/255f)
    private var paused: Boolean = false

    private lateinit var leftButton: LeftTurn
    private lateinit var rightButton: RightTurn
    private lateinit var thrustButton: LeftTurn

    init {
        init_game()
    }

    private fun init_game() {
        controller = WorldController(game)
        renderer = WorldRender(controller)
        uiInteface = UIInterface()

        renderer.set_ui(uiInteface)


        leftButton = LeftTurn(50f,-100f,50f)
        uiInteface.addButton(leftButton)
        rightButton = RightTurn(175f,-100f,50f)
        uiInteface.addButton(rightButton)
        thrustButton = LeftTurn(-100f,-50f,50f)
        uiInteface.addButton(thrustButton)

        controller.rocket.setLeftTurnHandler(object: ButtonHandler {
            override fun pressed(): Boolean { return leftButton.pressed }})
        controller.rocket.setRightTurnHandler(object: ButtonHandler {
            override fun pressed(): Boolean { return rightButton.pressed }})

        controller.rocket.setThrustHandler(object: ButtonHandler {
            override fun pressed(): Boolean { return thrustButton.pressed }})

        val multiInput: InputMultiplexer = InputMultiplexer()
        multiInput.addProcessor(uiInteface)
        multiInput.addProcessor(controller)
        Gdx.input.setInputProcessor(multiInput)
    }


    override fun render(deltaTime: Float) {
        if(!paused) {
            controller.update(deltaTime)
        }
        Gdx.gl.glClearColor(backgroundColor[0], backgroundColor[1], backgroundColor[2],
                0xff/255.0f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.render();
    }

    override fun resize(width: Int, height: Int) {
        renderer.resize(width,height)
    }

    override fun show() {
        init_game()
    }

    override fun hide() {
        renderer.dispose()
    }

    override fun pause() {
        this.paused = true
    }

    override fun resume() {
        this.paused = false
    }
}