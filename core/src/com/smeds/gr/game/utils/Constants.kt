package com.smeds.gr.game.utils

/**
 * Created by patsm159 on 2017-12-03.
 */
object Constants {
    val VIEWPORT_WIDTH: Float = 5.0f

    val VIEWPORT_HEIGHT: Float = 5.0f

//    val TEXTURE_ATLAS_OBJECTS = "images/gravityescape.pack.atlas"

//    val LEVEL_01 = "levels/level-01.png"

    val VIEWPORT_GUI_WIDTH: Float = 1280.0f

    val VIEWPORT_GUI_HEIGHT: Float = 720.0f

    var ASPECT_RATIO: Float = VIEWPORT_GUI_WIDTH / VIEWPORT_GUI_HEIGHT

    val LIVES_START: Int = 3

    val ITEM_FEATHER_POWERUP_DURATION: Float = 9f
}