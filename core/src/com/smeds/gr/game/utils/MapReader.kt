package com.smeds.gr.game.utils

import com.smeds.gr.game.objects.GoalObject
import com.smeds.gr.game.objects.PlanetObject
import com.smeds.gr.game.objects.RocketObject

/**
 * Created by patrik on 2018-01-27.
 */
class MapReader(val rocket: RocketObject, val goal: GoalObject, val planets: List<PlanetObject>) {
}