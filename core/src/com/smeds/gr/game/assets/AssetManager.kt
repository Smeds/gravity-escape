package com.smeds.gr.game.assets

//import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import javax.xml.soap.Text


/**
 * Created by patsm159 on 2017-12-08.
 */
class AssetManager {

    val manager = com.badlogic.gdx.assets.AssetManager()

    lateinit var defaultSmall: BitmapFont
    lateinit var defaultNormal: BitmapFont
    lateinit var defaultBig: BitmapFont

    fun loadFonts() {
        manager.load("fonts/arial-15.fnt", BitmapFont::class.java)
        manager.finishLoading()
        defaultSmall = BitmapFont(Gdx.files.internal("fonts/arial-15.fnt"), true)
        defaultNormal = BitmapFont(Gdx.files.internal("fonts/arial-15.fnt"), true)
        defaultBig = BitmapFont(Gdx.files.internal("fonts/arial-15.fnt"), true)

        defaultSmall.data.setScale(0.75f)
        defaultNormal.data.setScale(1.0f)
        defaultBig.data.setScale(2.0f)

        defaultSmall.region.texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
        defaultNormal.region.texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
        defaultBig.region.texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)


    }
}