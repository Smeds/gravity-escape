package com.smeds.gr.game

import com.smeds.gr.game.objects.AbstractGameObject
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.MathUtils





/**
 * Created by patsm159 on 2017-11-12.
 */
class CameraHelper {
    private val TAG = CameraHelper::class.java!!.getName()

    private var MAX_ZOOM_IN = 0.25f
    private var MAX_ZOOM_OUT = 10.0f

    private val position: Vector2 = Vector2()
    private var zoom: Float = 1.0f
    private var target: AbstractGameObject? = null

    fun update(deltaTime: Float) {
        if (!hasTarget()) return

        position.x = target!!.position.x + target!!.origin!!.x
        position.y = target!!.position.y + target!!.origin!!.y
    }

    fun setPosition(x: Float, y: Float) {
        this.position.x = x
        this.position.y = y
    }

    fun getPosition(): Vector2 {
        return this.position
    }

    fun addZoom(amount: Float) {
        setZoom(zoom + amount)
    }

    fun setZoom(zoom: Float) {
        this.zoom = MathUtils.clamp(zoom, MAX_ZOOM_IN, MAX_ZOOM_OUT)
    }

    fun getZoom(): Float {
        return this.zoom
    }

    fun setTarget(t: AbstractGameObject) {
        this.target = t
    }

    fun getTarget(): AbstractGameObject {
        return this.target!!
    }

    fun hasTarget(target: AbstractGameObject): Boolean {
        return hasTarget() && this.target!!.equals(target)
    }

    fun hasTarget(): Boolean {
        return target != null
    }

    fun isTarget(s: Sprite): Boolean {
        return this.hasTarget() && this.target!!.equals(s)
    }

    fun applyTo(camera: OrthographicCamera) {
        camera.position.x = this.position.x
        camera.position.y = this.position.y
        camera.zoom = this.zoom
        camera.update()
    }
}