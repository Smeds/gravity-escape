package com.smeds.gr.game

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.smeds.gr.game.utils.Constants
import com.smeds.gr.game.assets.AssetManager
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.smeds.gr.game.gui.UIInterface


/**
 * Created by patsm159 on 2017-11-12.
 */
class WorldRender(worldController: WorldController) {
    private lateinit var camera: OrthographicCamera
    private lateinit var batch: SpriteBatch
    lateinit var srend: ShapeRenderer
    private var worldController: WorldController
    var assetManager: AssetManager

    var pixmap: Pixmap = Pixmap(256,256,Pixmap.Format.RGBA8888)
    lateinit var texture: Texture

    private lateinit  var cameraGUI: OrthographicCamera

    lateinit var gui_interface: UIInterface

    init {
        //ToDo add loading screen
        assetManager = AssetManager()
        assetManager.loadFonts()
        assetManager.manager.finishLoading()
        this.worldController = worldController
        init()
    }

    fun set_ui(ui_interface: UIInterface) {
        this.gui_interface = ui_interface
        this.gui_interface.set_camera(cameraGUI)
    }

    private fun init() {
        batch = SpriteBatch();
        srend = ShapeRenderer()
        srend.setAutoShapeType(true)
        camera = OrthographicCamera(Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT)

        camera.position.set(0f, 0f, 0f)
        camera.zoom = 1.3f
        camera.update()

        cameraGUI = OrthographicCamera(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT)
        cameraGUI.position.set(0f, 0f, 0f)
        cameraGUI.setToOrtho(true) // flip y-axis
        cameraGUI.update()
    }

    fun render() {
        renderWorld(batch,Gdx.graphics.deltaTime)
        renderWorld(srend,Gdx.graphics.deltaTime)
        renderGui(batch)
        debug_render(srend,Gdx.graphics.deltaTime)

        worldController.rocket.appliedForce.x = 0f
        worldController.rocket.appliedForce.y = 0f
    }

    fun debug_render(shapeRender: ShapeRenderer,deltaTime: Float) {
        shapeRender.projectionMatrix = camera.combined
        shapeRender.begin()
        worldController.rocket.draw_line(shapeRender,deltaTime)
        worldController.rocket.draw_boundary_box(shapeRender,deltaTime)
        shapeRender.end()
        if(worldController.level.planets.size > 0 ) {
            shapeRender.begin(ShapeRenderer.ShapeType.Line);
            worldController.level.planets[0].draw_boundary_box(shapeRender,deltaTime)
            shapeRender.end()
        }
    }

    private fun renderWorld(batch: SpriteBatch, deltaTime: Float) {
        worldController.cameraHelper.applyTo(camera)
        batch.projectionMatrix = camera.combined
        batch.begin()
        //for (sprite in worldController.testSprites) {
        //    sprite!!.draw(batch)
        //}
        worldController.level.render(batch,deltaTime)
        worldController.rocket.render(batch,deltaTime)
        batch.end()
    }

    private fun renderWorld(shapeRender: ShapeRenderer, deltaTime: Float) {
        shapeRender.projectionMatrix = camera.combined
        shapeRender.begin(ShapeRenderer.ShapeType.Filled)
        worldController.level.goal.render(shapeRender,deltaTime)
        shapeRender.end()


    }

    private fun renderGui(batch: SpriteBatch) {
        batch.projectionMatrix = cameraGUI.combined
        batch.begin()

        // draw collected gold coins icon + text
        // (anchored to top left edge)
        //renderGuiScore(batch)
        // draw extra lives icon + text (anchored to top right edge)
        //renderGuiExtraLive(batch)
        // draw FPS text (anchored to bottom right edge)
        renderGuiFpsCounter(batch)
        drawPosition(batch)
        drawSpeed(batch)
        drawForce(batch)
        //renderTouchInterface(srend)
        gui_interface.render(srend)
        batch.end()

    }

    private fun renderTouchInterface(shapeRender: ShapeRenderer) {
        shapeRender.projectionMatrix = cameraGUI.combined
        val x = cameraGUI.viewportWidth - 60
        val y = cameraGUI.viewportHeight
        shapeRender.begin(ShapeRenderer.ShapeType.Filled)
        shapeRender.setColor(0f, 128f/255f, 128f/255f, 0.5f)
        shapeRender.rect(x-20,y-100,40f,80f)
        shapeRender.setColor(0f, 128f/255f, 128f/255f, 0.5f)
        shapeRender.circle(20f,y-50,10f)
        shapeRender.circle(45f,y-50,10f)
        shapeRender.end()
    }

    private fun renderGuiScore(batch: SpriteBatch) {
        val x: Float = -15f
        val y: Float = -15f
        val font: BitmapFont = assetManager.manager.get("fonts/arial-15.fnt", BitmapFont::class.java)
        font.draw(batch, "" + worldController.score,x + 75.0f, y + 37.0f)//Assets.instance.fonts.defaultBig.draw(batch,
    }

    /*private fun renderGuiExtraLive(batch: SpriteBatch) {
        val x = cameraGUI!!.viewportWidth - 50 - Constants.LIVES_START * 50
        val y = -15f
        for (i in 0 until5 Constants.LIVES_START) {
            if (worldController.lives <= i)
                batch.setColor(0.5f, 0.5f, 0.5f, 0.5f)
            batch.draw(Assets.instance.bunny.head,
                    x + i * 50, y, 50f, 50f, 120f, 100f, 0.35f, -0.35f, 0f)
            batch.setColor(1f, 1f, 1f, 1f)
        }
    }*/

    private fun renderGuiFpsCounter(batch: SpriteBatch) {
        val x = cameraGUI.viewportWidth - 55
        val y = cameraGUI.viewportHeight - 15
        val fps = Gdx.graphics.framesPerSecond
        val fpsFont: BitmapFont = assetManager.defaultSmall
        if (fps >= 45) {
            // 45 or more FPS show up in green
            fpsFont.setColor(0f, 1f, 0f, 1f)
        } else if (fps >= 30) {
            // 30 or more FPS show up in yellow
            fpsFont.setColor(1f, 1f, 0f, 1f)
        } else {
            // less than 30 FPS show up in red
            fpsFont.setColor(1f, 0f, 0f, 1f)
        }
        fpsFont.draw(batch, "FPS: " + fps, x, y)
        fpsFont.setColor(1f, 1f, 1f, 1f) // white*/
    }

    private fun drawPosition(batch: SpriteBatch) {
        val x = cameraGUI.viewportWidth - 160
        val y = cameraGUI.viewportHeight - 15
        val fps = Gdx.graphics.framesPerSecond
        val fpsFont: BitmapFont = assetManager.defaultSmall
        fpsFont.setColor(0f, 1f, 0f, 1f)

        fpsFont.draw(batch, "[ %.2f, %.2f ]".format(worldController.rocket.position.x, worldController.rocket.position.y), x, y)
        fpsFont.setColor(1f, 1f, 1f, 1f) // white*/
    }

    private fun drawSpeed(batch: SpriteBatch) {
        val x = cameraGUI.viewportWidth - 200
        val y = cameraGUI.viewportHeight - 15
        val fps = Gdx.graphics.framesPerSecond
        val fpsFont: BitmapFont = assetManager.defaultSmall
        fpsFont.setColor(0f, 1f, 0f, 1f)

        fpsFont.draw(batch, "%.2f".format(worldController.rocket.momentum.len()), x, y)
        fpsFont.setColor(1f, 1f, 1f, 1f) // white*/
    }

    private fun drawForce(batch: SpriteBatch) {
        val x = cameraGUI.viewportWidth - 300
        val y = cameraGUI.viewportHeight - 15
        val fps = Gdx.graphics.framesPerSecond
        val fpsFont: BitmapFont = assetManager.defaultSmall
        fpsFont.setColor(0f, 1f, 0f, 1f)

        fpsFont.draw(batch, "[ %.3f, %.3f ]".format(worldController.rocket.appliedForce.x, worldController.rocket.appliedForce.y), x, y)
        fpsFont.setColor(1f, 1f, 1f, 1f) // white*/
    }


    fun resize(width: Int, height: Int) {
        camera.viewportWidth = (Constants.VIEWPORT_HEIGHT / height) * width
        camera.update()

        cameraGUI.viewportHeight = Constants.VIEWPORT_GUI_HEIGHT
        cameraGUI.viewportWidth = Constants.VIEWPORT_GUI_HEIGHT / height.toFloat() * width.toFloat()
        cameraGUI.position.set(cameraGUI.viewportWidth / 2,
               cameraGUI.viewportHeight / 2, 0f)
        cameraGUI.update()

        gui_interface.update()
    }

    fun dispose() {
        batch.dispose()
    }
}