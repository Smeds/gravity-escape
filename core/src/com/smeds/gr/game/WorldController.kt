package com.smeds.gr.game

import com.badlogic.gdx.*
import com.beust.klaxon.Klaxon
import com.smeds.gr.game.levels.Level
import com.smeds.gr.game.utils.Constants
import com.badlogic.gdx.graphics.g2d.Sprite
import com.smeds.gr.game.objects.AbstractGameObject
import com.smeds.gr.game.objects.PlanetObject
import com.smeds.gr.game.objects.RocketObject
import com.smeds.gr.game.utils.MapReader
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Intersector
import com.smeds.gr.game.screens.MenuScreen


/**
 * Created by patsm159 on 2017-11-12.
 */
class WorldController(private val game: Game): InputAdapter() {
    val TAG = WorldController::class.java.name

    var lives: Int = 0
    var score: Int = 0
    lateinit var level: Level
    lateinit var rocket: RocketObject

    var cameraHelper: CameraHelper = CameraHelper()

    var testSprites: Array<Sprite?> = arrayOfNulls<Sprite>(5)
    var selectedSprite: Int = 0
    var physicsHandler: PhysicsHandler = PhysicsHandler()

    init {
        initialize()
    }

    private fun initialize() {
        initMap("levels/level1.json")
        cameraHelper = CameraHelper()
        cameraHelper.setZoom(2.0f)
        lives = Constants.LIVES_START
        cameraHelper.setTarget(rocket)
        physicsHandler.movingObjects.add(rocket)

    }

    fun backToMenu() {
        game.setScreen(MenuScreen(game))
    }

    private fun initMap(map: String) {
        val inputLevel: String = Gdx.files.internal(map).readString()


        var map = Klaxon().parse<MapReader>(inputLevel)
        rocket = map!!.rocket
        level = Level(map!!.planets, map!!.goal)
        for(p: PlanetObject in level.planets) {
            physicsHandler.addGameObjects(p)
        }
    }

    private fun initLevel() {
        //level = Level()
        for(p: AbstractGameObject? in level.planets) {
            physicsHandler.addGameObjects(p!!)
        }

    }

    fun collision_detection() {
        var collided: Int = 0
        for(obj: PlanetObject in level.planets) {
            if(obj.bounds.overlaps(rocket.bounds)) {
                if(circle_polygon_collision(rocket.polygon.transformedVertices, obj.center.x, obj.center.y, obj.r)) {
                    collided = 2
                    break
                }
                collided = 1
            }
        }
        if(level.goal.bounds.overlaps(rocket.bounds)) {
            backToMenu()
        }
        rocket.collision(collided)
    }

    private var center: Vector2 = Vector2()

    fun circle_polygon_collision(vertices: FloatArray, circe_position_x: Float, circle_position_y: Float, radius: Float): Boolean {
        var squared_radius = radius*radius
        center.x = circe_position_x
        center.y = circle_position_y
        for(i in 0..(vertices.size-1) step 2) {
            if (i === 0) {
                if (Intersector.intersectSegmentCircle(Vector2(vertices[vertices.size - 2],
                        vertices[vertices.size - 1]), Vector2(vertices[i], vertices[i + 1]),
                        center, squared_radius)) {
                    return true
                }
            } else {
                if (Intersector.intersectSegmentCircle(Vector2(vertices[i - 2], vertices[i - 1]),
                        Vector2(vertices[i], vertices[i + 1]),
                        center,
                        squared_radius)) {
                    return true
                }
            }
        }
        return false
    }

    fun update(deltaTime: Float) {
        physicsHandler.update(deltaTime)
        rocket.update(deltaTime)
        handleDebugInput(deltaTime)
        cameraHelper.update(deltaTime)
        collision_detection()
    }

    fun handleDebugInput(deltaTime: Float) {
        if (Gdx.app.type != Application.ApplicationType.Desktop) {
            return
        }

        var camMoveSpeed = 5 * deltaTime
        val camMoveSpeedAccelerationFactor = 5f
        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
            backToMenu()
        }
        if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
            camMoveSpeed *= camMoveSpeedAccelerationFactor
        }
        if (Gdx.input.isKeyPressed(Input.Keys.C)) {
            rocket.rotateCounterClockwise()
        }
        if(Gdx.input.isKeyPressed(Input.Keys.V)) {
            rocket.rotateClockwise()
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            moveCamera(-camMoveSpeed, 0.0f)
        }
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            moveCamera(camMoveSpeed, 0.0f)
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            moveCamera(0.0f, camMoveSpeed)
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            moveCamera(0.0f, -camMoveSpeed)
        }
        /*if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            rocket.throttle_pressed(1.0f)
        }
        if (Gdx.input.isKeyPressed(Input.Keys.F)) {
            rocket.throttle_pressed(0.0f)
        }*/
        if (Gdx.input.isKeyPressed(Input.Keys.BACKSPACE)) {
            moveCamera(0.0f, 0.0f)
        }

        var camZoomSpeed = 1.0f * deltaTime
        val camZoomSpeedAccelerationFactor = 5f
        if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
            camZoomSpeed *= camZoomSpeedAccelerationFactor
        }
        if (Gdx.input.isKeyPressed(Input.Keys.COMMA)) {
            cameraHelper.addZoom(camZoomSpeed)
        }
        if (Gdx.input.isKeyPressed(Input.Keys.PERIOD)) {
            cameraHelper.addZoom(-camZoomSpeed)
        }
        if (Gdx.input.isKeyPressed(Input.Keys.NUM_7)) {
            cameraHelper.setZoom(1.0f)
        }
    }

    private fun moveCamera(x: Float, y: Float) {
        var x = x
        var y = y
        x += cameraHelper.getPosition().x
        y += cameraHelper.getPosition().y
        cameraHelper.setPosition(x, y)
    }

    override fun keyUp(keycode: Int): Boolean {
        if (keycode == Input.Keys.R) {
            initialize()
            Gdx.app.debug(TAG, "Game world resetted!")
        }
        return false
    }
}