package com.smeds.gr.game.gui

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Vector3
import com.smeds.gr.game.utils.Constants

/**
 * Created by patsm159 on 2018-03-28.
 */

interface LeftButtonListener {
    fun left_pressed(pressed: Boolean)
}

interface RightButtonListener {
    fun right_pressed(pressed: Boolean)
}

interface ThrottleButtonListener {
    fun throttle_pressed(level: Float)
}

class UIInterface: InputProcessor {
    private var width: Int = 0
    private var height: Int = 0

    /*private lateinit var throttle: Throttle

    private lateinit var left_turn: Button
    private lateinit var right_turn: Button*/

    //private var r: Float = 30f
    /*private var width_throttle: Float = 35f
    private var height_throttle: Float = 160f
    private var distance: Float = 10f*/

    /*private var left_buttonListener: LeftButtonListener? = null
    private var right_buttonListener: RightButtonListener? = null
    private var throttle_buttonListener: ThrottleButtonListener? = null*/

    private var pointer_list: List<Int> = listOf<Int>()

    private var buttonList: MutableList<Button> = mutableListOf<Button>()

    private lateinit var cameraGUI: OrthographicCamera

    init {
        update()
    }

    fun set_camera(camera: OrthographicCamera) {
        this.cameraGUI = camera
        this.width = Constants.VIEWPORT_GUI_WIDTH.toInt()
        this.height = Constants.VIEWPORT_GUI_HEIGHT.toInt()
    }

    fun addButton(button:Button) {
        print("Height: " + height + " width " + width + " " + button.x + " " + button.y + "\n\n")
        if(button.x < 0)
            button.x += width
        if(button.y < 0)
            button.y += height
        print("\t" + button.x + " " + button.y + "\n\n")
        buttonList.add(button)
    }

    fun render(srenderer: ShapeRenderer) {
        srenderer.projectionMatrix = cameraGUI.combined
        for (b in buttonList)
            b.render(srenderer)
    }

    fun update() {
        /*throttle = Throttle(cameraGUI.viewportWidth - distance - width_throttle,cameraGUI.viewportHeight -distance,width_throttle, height_throttle)
        left_turn = LeftTurn(distance+r,cameraGUI.viewportHeight - distance - r, r)
        right_turn = RightTurn((distance+r)*3,cameraGUI.viewportHeight - distance -r, r)*/
    }

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        val coords: Vector3 = cameraGUI.unproject(Vector3(screenX.toFloat(), screenY.toFloat(),1f))
        for (b in buttonList) {
            if(b.controll_pressed(coords.x.toInt(),coords.y.toInt(),pointer))
                return true
        }
        return false
    }

    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        val coords: Vector3 = cameraGUI.unproject(Vector3(screenX.toFloat(), screenY.toFloat(),1f))
        for (b in buttonList) {
            if(b.controll_released(coords.x.toInt(),coords.y.toInt(),pointer))
                return true
        }
        return false
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean { return false }
    /*override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        val coords: Vector3 = cameraGUI.unproject(Vector3(screenX.toFloat(), screenY.toFloat(),1f))
        if(throttle.controll_dragged(coords.x.toInt(),coords.y.toInt(),pointer)) {
            this.throttle_buttonListener?.throttle_pressed(throttle.level)
            return true
        } else if(left_turn.controll_dragged(coords.x.toInt(),coords.y.toInt(),pointer)) {
            this.left_buttonListener?.left_pressed(left_turn.pressed)
            return true
        } else if(right_turn.controll_dragged(coords.x.toInt(),coords.y.toInt(),pointer)) {
            this.right_buttonListener?.right_pressed(right_turn.pressed)
            return true
        }
        return false
    }*/

    override fun keyTyped(character: Char): Boolean {
        return false
    }

    override fun scrolled(amount: Int): Boolean {
        return false
    }

    override fun keyUp(keycode: Int): Boolean {
        return false
    }

    override fun keyDown(keycode: Int): Boolean {
        return false
    }

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        return false
    }
}