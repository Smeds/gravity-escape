package com.smeds.gr.game.gui

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.glutils.ShapeRenderer

/**
 * Created by patsm159 on 2018-03-28.
 */
open class Button(var x: Float, var y: Float, val r: Float) {
    var pressed: Boolean = false
    var pointer: Int = -1

    open fun render(shapeRenderer: ShapeRenderer) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
        if(pressed) {
            shapeRenderer.setColor(255f / 255f, 209f / 255f, 204f / 255f, 0.5f)
        } else {
            shapeRenderer.setColor(72f / 255f, 209f / 255f, 204f / 255f, 0.5f)
        }
        shapeRenderer.circle(x,y,r)
        shapeRenderer.end()
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line)
        Gdx.gl20.glLineWidth(5f)
        if(pressed) {
            shapeRenderer.setColor(1f, 128f/255f, 128f/255f, 0.5f)
        } else {
            shapeRenderer.setColor(0f, 128f/255f, 128f/255f, 0.5f)
        }

        shapeRenderer.circle(x,y,r)
        shapeRenderer.end()
        Gdx.gl20.glLineWidth(1f)
    }

    fun controll_pressed(x: Int, y: Int, pointer: Int): Boolean {
        if (!this.pressed && Math.sqrt(((this.x-x)*(this.x-x) + (this.y-y)*(this.y-y)).toDouble()) < r) {
            this.pressed = true
            this.pointer = pointer
            return true
        }
        return false
    }

    fun controll_released(x: Int, y: Int, pointer: Int): Boolean {
        if (this.pressed && this.pointer == pointer && Math.sqrt(((this.x-x)*(this.x-x) + (this.y-y)*(this.y-y)).toDouble()) < r) {
            this.pressed = false
            this.pointer = -1
            return true
        }
        return false
    }

    fun controll_dragged(x: Int, y: Int, pointer: Int): Boolean {
        if(pressed && this.pointer == pointer) {
            if (Math.sqrt(((this.x-x)*(this.x-x) + (this.y-y)*(this.y-y)).toDouble()) > r) {
                pressed = false
                this.pointer = -1
                return true
            }
        }
        return false
    }
}