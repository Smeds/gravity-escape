package com.smeds.gr.game.gui

import com.badlogic.gdx.graphics.glutils.ShapeRenderer

/**
 * Created by patsm159 on 2018-03-28.
 */
class Throttle(val x: Float, val y: Float,  val width: Float, val height: Float) {
    var pressed: Boolean = false
    var pointer: Int = -1
    var level: Float = 0f

    fun render(shapeRenderer: ShapeRenderer) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
        if (pressed) {
            shapeRenderer.setColor(255f / 255f, 209f / 255f, 204f / 255f, 0.5f)
        } else {
            shapeRenderer.setColor(72f / 255f, 209f / 255f, 204f / 255f, 0.5f)
        }
        shapeRenderer.rect(x, y - height, width, height)
        shapeRenderer.end()
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line)
        if (pressed) {
            shapeRenderer.setColor(1f, 128f / 255f, 128f / 255f, 0.5f)
        } else {
            shapeRenderer.setColor(0f, 128f / 255f, 128f / 255f, 0.5f)
        }
        shapeRenderer.rect(x, y - height, width, height)
        shapeRenderer.end()
    }

    fun controll_pressed(x: Int, y: Int, pointer: Int): Boolean {
        if (!pressed && this.x < x && x < this.x + this.width &&
                this.y - this.height < y && y < this.y) {
            pressed = true
            this.pointer = pointer
            level = (this.y - y)/this.height
            return true
        }
        return false
    }

    fun controll_released(x: Int, y: Int, pointer: Int): Boolean {
        if (pressed && this.pointer == pointer &&
                this.x < x && x < this.x + this.width &&
                this.y - this.height < y && y < this.y) {
            pressed = false
            this.pointer = -1
            level = 0f
            return true
        }
        return false
    }

    fun controll_dragged(x: Int, y: Int, pointer: Int): Boolean {
        if (pressed && this.pointer == pointer) {
            if (!(this.x < x && x < this.x + this.width &&
                            this.y - this.height < y && y < this.y)) {
                pressed = false
                this.pointer = -1
                return true
            } else {
                level = (this.y - y)/this.height
            }
        }
        return false
    }
}