package com.smeds.gr.game.gui

import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import java.awt.geom.Path2D

/**
 * Created by patsm159 on 2018-03-28.
 */
class RightTurn: Button {
    constructor(x: Float, y: Float, r: Float): super(x,y,r) {}

    override fun render(shapeRenderer: ShapeRenderer) {
        super.render(shapeRenderer)
        shapeRenderer.begin()
        shapeRenderer.line(x,y,x-r/2,y-r/2)
        shapeRenderer.line(x,y,x-r/2,y+r/2)
        shapeRenderer.line(x+r/2,y,x,y-r/2)
        shapeRenderer.line(x+r/2,y,x ,y+r/2)
        shapeRenderer.end()
    }
}