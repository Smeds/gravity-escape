package com.smeds.gr.game.levels

import com.smeds.gr.game.objects.PlanetObject
import com.smeds.gr.game.objects.RocketObject
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.smeds.gr.game.objects.AbstractGameObject
import com.beust.klaxon.Json
import com.smeds.gr.game.objects.GoalObject

/**
 * Created by patsm159 on 2017-12-03.
 */
class Level(val planets: List<PlanetObject>, val goal: GoalObject) {
    val TAG = Level::class.java.name

    //var planets: Array<AbstractGameObject?> = arrayOfNulls<AbstractGameObject>(1)

    //init {
    //    planets[0] = PlanetObject(-0.5f,-0.5f,20)
    //}

    fun render(batch: SpriteBatch,deltaTime: Float) {
        for (p in planets) {
            p.render(batch,deltaTime)
        }
    }

    fun render(shapeRenderer: ShapeRenderer,deltaTime: Float) {
        goal.render(shapeRenderer,deltaTime)
    }
}