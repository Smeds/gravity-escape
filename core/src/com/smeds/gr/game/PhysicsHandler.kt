package com.smeds.gr.game

import com.badlogic.gdx.math.Vector2
import com.smeds.gr.game.objects.AbstractGameObject
import com.smeds.gr.game.objects.RocketObject

/**
 * Created by patsm159 on 2017-12-15.
 */
class PhysicsHandler {
    val movingObjects : MutableList<AbstractGameObject> = arrayListOf()
    val gravityObjects : MutableList<AbstractGameObject> = arrayListOf()

    fun addGameObjects(gameObject: AbstractGameObject) {
        if(gameObject.hasGravity()) {
            gravityObjects.add(gameObject)
        }
        if(gameObject.moveable) {
            movingObjects.add(gameObject)
        }
    }

    fun update(deltaTime: Float) {
        for (gravityObject in gravityObjects) {
            for (movingObject in movingObjects) {
                var norm = Vector2(movingObject.position).nor()
                var x = (movingObject.position.x + movingObject.origin.x) - (gravityObject.position.x + gravityObject.origin.x)
                var y = (movingObject.position.y + movingObject.origin.y) - (gravityObject.position.y + gravityObject.origin.y)
                var len2 = (x*x+y*y).toDouble()
                var len = Math.sqrt(len2)
                var constant = (gravityObject.mass*gravityObject.gravity/len2)/len
                movingObject.addForce(Vector2((-x*constant).toFloat(),(-y*constant).toFloat()))
            }
        }
    }
}