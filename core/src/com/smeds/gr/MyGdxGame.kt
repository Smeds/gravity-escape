package com.smeds.gr

import com.badlogic.gdx.*
import com.badlogic.gdx.graphics.GL20
import com.smeds.gr.game.WorldController
import com.smeds.gr.game.WorldRender
import com.smeds.gr.game.screens.MenuScreen


class MyGdxGame : Game() {
    override fun create() {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        setScreen(MenuScreen(this))
    }
}
